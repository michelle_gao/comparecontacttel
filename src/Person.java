import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michelle on 2/6/14.
 */
public class Person {
    private String ownName;
    private String telNumber;
    private HashMap<String, Person> contacts;
    public Person() {
        contacts = new HashMap<String, Person>();
    }


    public String getOwnName() {
        return ownName;
    }

    public void setOwnName(String ownName) {
        this.ownName = ownName;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public HashMap<String, Person> getContacts() {
        return contacts;
    }

    public void setContacts(String friendName, Person friend) {
        contacts.put(friendName, friend);
    }


}
