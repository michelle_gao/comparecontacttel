import java.util.HashMap;

/**
 * Created by Michelle on 2/6/14.
 * Compare if my friend has my right Tel number
 */
public class Utility {

    public Utility() {}
    public static boolean compareTel(Person me, String friendName) {
        HashMap<String,Person> myContact =me.getContacts();
        Person mFriend = myContact.get(friendName);
        HashMap<String,Person> friendContacts = mFriend.getContacts();
        Person meFromFriend = friendContacts.get(me.getOwnName());
        if (me.getTelNumber().equals(meFromFriend.getTelNumber())) {
            return true;
        }
        else return false;
    }
}
