import java.util.*;

/**
 * Created by Michelle on 2/6/14.
 */
public class CompareContactTel {
    public static void main(String[] args) {
        Person me = new Person();
        me.setOwnName("Michelle");
        me.setTelNumber("7733188970");
        Person friend = new Person();
        friend.setOwnName("Jennifer");
        friend.setTelNumber("7739208920");
        me.setContacts("Jennifer",friend);
        friend.setContacts("Michelle", me);
        if (Utility.compareTel(me, "Jennifer")) {
            System.out.println(" My Friend has my right number");
        }
        else System.out.println(" My friend has my wrong number");
    }
}
